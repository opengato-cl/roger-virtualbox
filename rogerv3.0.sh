#!/bin/bash
# ROGER v.3
# ok Virtualbox 4.1.18/OS X
# ok Virtualbox 4.1.20/OS X 
# ok Virtualbox 4.1.12/Ubuntu 12.04
# ok Virtualbox 6.0/OSX
#
# -------------------------
# Copyright (C)
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
# -------------------------
clear
while true
do
echo " "
echo "                         *********************************************"
echo "                         *                                           *"
echo "                         *    Consola de Administración ROGER V.3.0  *"
echo "                         *    GPL V.2                                *"
echo "                         *    roger@opengato.cl                      *"
echo "                         *                                           *"
echo "                         *********************************************"
echo ""
echo "1) Listar VM"
echo "2) Listar VM Corriendo"
echo "3) Levantar Interfaz Gráfica de Administración"
echo "4) Información del Host"
echo "5) Información VM"
echo "6) Levantar VM"
echo "7) Cambiar Nombre VM"
echo "8) Clonar Máquina"
echo "9) Apagar VM"
echo "10) Crear VM"
echo "11) Borrar VM"
echo "12) Acceder a Consola (VM debe esta down)"
echo "13) Eject CD/DVD"
echo "14) Salir"
echo ""
read -p "Elija opcion: " opcion
case $opcion in
1) clear
echo "Listado VMs:"
echo ""
VBoxManage list vms ;;
2) clear
echo "VMs Corriendo:"
echo ""
VBoxManage list  runningvms 
echo ""
echo "Memoria Utilizada:"
echo ""
SO=`uname`
if [ "Darwin" = "$SO" ]; then
               open /Applications/Utilities/Activity\ Monitor.app
            else
               free -m
            fi;;
3) clear
virtualbox &;;
4) clear
VBoxManage list hostinfo ;;
5)clear
echo "Listado de VM :)"
echo ""
VBoxManage list vms
echo ""
read -p "Ingrese VM a mostrar: " vminforst
VBoxManage showvminfo $vminforst | grep VRDE
echo ""
VBoxManage showvminfo $vminforst;; 
6) clear
echo "VMs Disponibles :)"
echo ""
VBoxManage list vms
echo ""
read -p "Ingrese Nombre de VM:" namevm
SO=`uname`
if [ "Darwin" = "$SO" ]; then
            VBoxManage startvm $namevm -type headless &
            else
read -p "Ingrese Puerto (3389-xxxx):" puerto
            VBoxHeadless -startvm $namevm -p $puerto &
            fi;;
7) clear
echo ""
echo "VMs Disponibles :)"
echo ""
VBoxManage list vms
echo ""
read -p "Cambiar Nombre a: " vmnameold
read -p "Nuevo Nombre VM (Recuerde utilizar _ en vez de espacio en blanco): " vmnamenew
VBoxManage modifyvm $vmnameold --name $vmnamenew
clear
echo ""
echo "El nombre de VM ha sido cambiado"
echo ""
VBoxManage list vms
;;
8) clear
echo ""
echo "VMs Disponibles :)"
echo ""
VBoxManage list vms
echo ""
read -p "Ingrese VM a Clonar: " vmclon
vboxmanage clonevm $vmclon --name $vmclon"(clonada)" --register
clear
echo ""
echo "VMs Disponibles :)"
echo ""
VBoxManage list vms
;;
9) clear
echo "VM Corriendo :p"
echo ""
VBoxManage list runningvms
echo ""
read -p "Ingrese Nombre de VM:" closevm
VBoxManage controlvm $closevm poweroff;;
10) clear
echo ""
read -p "Nombre de VM Nueva: " vmnew
read -p "Tamaño Disco Duro (MB): " hdnew
read -p "Path de Disco Duro: " pathhd
echo "S.O. Disponibles : "
echo ""
vboxmanage list ostypes | more
read -p "Sistema Operativo: " typeos
read -p "Memoria RAM (MB): " ram
read -p "Path ubicación de Imagen (ejemplo: /home/usuario/so.iso) : " path
read -p "Puerto VM (3389-xxxx): " puerto2
VBoxManage createvm -name $vmnew -ostype $typeos -register
VBoxManage createhd -filename $vmnew.vdi -size $hdnew -format VDI
VBoxManage storagectl $vmnew --name "SATA Controller" --add sata --controller IntelAhci
VBoxManage storageattach "$vmnew" --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $vmnew.vdi
VBoxManage modifyvm $vmnew --memory $ram
VBoxManage storagectl $vmnew --name "IDE Controller" --add ide --controller PIIX4
VBoxManage storageattach $vmnew --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium $path
VBoxManage modifyvm $vmnew --vrde on
VBoxManage modifyvm $vmnew --vrdemulticon on --vrdeport $puerto2
VBoxManage modifyvm $vmnew --nic1 bridged --nictype1 82545EM --bridgeadapter1 en1
VBoxManage modifyvm $vmnew --boot1 disk --boot2 dvd
VBoxManage showvminfo $vmnew | grep VRDE;;
11)clear
echo "Proceso Borrado :/"
echo ""
VBoxManage list vms
echo ""
read -p "VM a Borrar :/ :" deletevm
VBoxManage storagectl $deletevm --name "SATA Controller" --remove
VBoxManage unregistervm $deletevm --delete
VBoxManage closemedium disk $deletevm.vdi --delete
echo ""
VBoxManage list vms;;
12)clear
echo "Proceso Acceder a Consola por RDP :/"
echo ""
VBoxManage list vms
echo ""
read -p "VM ha Acceder :/ :" rdpvm
read -p "Ingrese Puerto :" puerto 
VBoxHeadless --startvm $rdpvm --vrde on --vrdeproperty "TCP/Ports=$puerto" &
echo "";;
13)clear
echo "Eject CD/DVD :p"
echo ""
VBoxManage list vms
echo ""
read -p "Nombre VM :/ :" namevmcd
read -p "Path CD/DVD :/ :" pathcd
VBoxManage storageattach $namevmcd --storagectl "IDE Controller" --device 0 --port 0 --type dvddrive --medium $pathcd
echo "";;
14) clear
echo "Adiós gordito"
break;;
*) echo "Opciones de 1 a 7 solamente";;
esac
done
exit 0 
